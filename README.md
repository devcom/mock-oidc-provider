# Mock OIDC Prvider

This project provides a OIDC provider that supports mock data and is intended to be used for local development. To the best of its ability, it seeks to reflect the capabilities provided by the VT Gateway service.

This tool is built on top of the widely adopted and supported [Node oidc-provider framework](https://github.com/panva/node-oidc-provider).

If clients are configured with no secrets, they are expected to be single-page apps (SPAs) and must use PKCE in order to complete the auth flow.

**NOTE: This is NOT intended to be used in any production environments. You have been warned!**


## Usage

After creating a config file ([documentation below](#configuration)), you can launch the mock OIDC provider.

```cli
docker run -dp 8080:8080 \
    -v /path/to/config.yaml:/tmp/config.yaml
    -e CONFIG_FILE_PATH=/tmp/config.yaml
    code.vt.edu:5005/devcom/mock-oidc-provider
```

After it's running, you can authenticate using any of the people you defined in your config file. Any password can be used for the user accounts.

### Docker Compose Example

In most situations, this service will be included in a Docker Compose file for an app under development. The following example uses the [Localhost Proxy](https://code.vt.edu/devcom/devcom-localhost-proxy) to provide a signed and trusted cert in front of the app.

```yaml
services:
  proxy:
    image: code.vt.edu:5005/devcom/devcom-localhost-proxy:traefik-1.7
    ports:
      - 80:80
      - 443:443
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      default:
        aliases:
          - oidc.localhost.devcom.vt.edu
  oidc:
    image: code.vt.edu/devcom/mock-oidc-provider
    volumes:
      - ./config.yaml:/tmp/config.yaml
    environment:
      CONFIG_FILE_PATH: /tmp/config.yaml
    labels:
      traefik.backend: oidc
      traefik.frontend.rule: Host:oidc.localhost.devcom.vt.edu
      traefik.port: 8080
```

## Authentication

Since this is a mock OIDC provider, all users and their attributes must be defined beforehand in config ([details below](#configuration)). This allows local dev and test environments to use persona-based development and testing and remove any need to store or share credentials.

When signing in, any username/password combination will work. However, the auth flow will completely only if the username matches a person's `pid` found in the config file. When presented with the scope authorization form, you can simply click through. We'll auto-accept those without the need for consent in the future.

![Screenshot of the login form](./docs/login-screenshot.png)

![Screenshot of the authorization form](./docs/authorize-screenshot.png)

> If you get a `grant request is invalid` error after authenticating, it is probably because the username was not found in the config. We're trying to figure out how to fail earlier, but waiting on input from the upstream tooling.


## Configuration

Configuration for people and clients is provided in either YAML or JSON structure. The path to the file is indicated using the `CONFIG_FILE_PATH` environment variable. The configuration structure is as follows:

| Key | Type | Description |
|-----|------|-------------|
| `issuer_url` | `string` | The full URI for the service (using the publicly accessible name) |
| `clients` | [`Client[]`](#client-configuration) | A list of clients and their config that can use the service |
| `people` | [`People[]`](#people-configuration) | A list of people and their properties |


### `Client` configuration

Each client _requires_ each of the following properties:

| Key | Type | Description |
|-----|------|-------------|
| `id` | `string` | The id of the client |
| `secret` | `string` | The secret of the client. Since this is mock, there is no data sensitivity issue here |
| `redirect_uris` | `string[]` | A list of authorized URIs that the user can be redirected to after authentication |


### `People` configuration

For people, all of the [supported Gateway scopes](https://gateway.login.vt.edu/.well-known/openid-configuration) can be specified. Simply add additional key/value pairs to add data for the desired scopes.

| Key | Type | Required? | Description |
|-----|------|-----------|-------------|
| `pid` | `string` | Yes | The pid of the user. Will also be used to generate an email when using the `email` scope |
| `groups` | `string[]` | No | The groups this user belongs to. Will be released using the `groupMembershipUugid` scope |
| `<any-other-scope-claim>` | `string` | No | To release additional scopes/claims, any arbitrary value can be used that maps to a supported scope |


### Sample Config (YAML)

```yaml
issuer_url: https://oidc.localhost.devcom.vt.edu

clients:
  - id: test
    secret: secret
    redirect_uris:
      - https://localhost.devcom.vt.edu
  - id: spa-app
    # Notice no secret here
    redirect_uris:
      - https://localhost.devcom.vt.edu/spa

people:
  japplessed:
    name: Johnny Appleseed
    groups:
      - admin
      - editor
```

### Sample Config (JSON)

```json
{
  "issuer_url": "https://oidc.localhost.devcom.vt.edu",
  "clients": [
    {
      "id": "test",
      "secret": "secret",
      "redirect_uris": [
        "https://localhost.devcom.vt.edu"
      ]
    }
  ],
  "people": {
    "jappleseed": {
      "name": "Johnny Appleseed",
      "groups": [
        "admin", "writer"
      ]
    }
  }
}
```

## Development

This project contains a `docker-compose.yml` file that can be used to start a local dev environment. It uses the `config-sample.yaml` file as its config and starts the OIDC server. File changes will automatically restart the server.

```cli
docker-compose up -d
```

The app will be available at [https://oidc.localhost.devcom.vt.edu](https://oidc.localhost.devcom.vt.edu). For convenience, a simple OIDC client is available at [https://client.localhost.devcom.vt.edu](https://client.localhost.devcom.vt.edu).
