FROM node:14.15-alpine AS base
WORKDIR /usr/src/app
COPY package.json yarn.lock ./

FROM base AS test
RUN yarn install
COPY ./spec ./spec
COPY ./src ./src
RUN yarn test

FROM base AS prod
RUN yarn install --production
COPY --from=test /usr/src/app/src ./src
CMD ["node", "/usr/src/app/src/index.js"]
