const fs = require("fs");
const configLoader = require("../src/configLoader");

const SAMPLE_CONFIG_JSON = {
  issuer_url: "https://localhost",
  clients: [
    {
      id: "test",
      secret: "secret",
      redirect_uris: ["https://app.localhost"],
    },
  ],
  people: [
    {
      name: "Johnny Appleseed",
      pid: "jappleseed",
    },
  ],
};

const SAMPLE_CONFIG_YAML = `
issuer_url: https://localhost
clients:
  - id: test
    secret: secret
    redirect_uris: ["https://app.localhost"]
people:
  - name: Johnny Appleseed
    pid: jappleseed
`;

beforeEach(() => {
  fs.writeFileSync(
    "/tmp/config-loader.json",
    JSON.stringify(SAMPLE_CONFIG_JSON)
  );
  fs.writeFileSync("/tmp/config-loader.yaml", SAMPLE_CONFIG_YAML);
  fs.writeFileSync("/tmp/config-loader.yml", SAMPLE_CONFIG_YAML);
});

afterEach(() => {
  const delFile = (filename) => {
    if (fs.existsSync(filename)) fs.unlinkSync(filename);
  };

  delFile("/tmp/config-loader.json");
  delFile("/tmp/config-loader.yaml");
  delFile("/tmp/config-loader.yml");
});

it("throws when invalid path", () => {
  try {
    configLoader.getConfig("invalid-path");
    fail("should have thrown");
  } catch (err) {
    expect(err.message).toContain("Unsupported config source");
  }
});

it("loads the data correctly from a JSON file", () => {
  const config = configLoader.getConfig("/tmp/config-loader.json");
  expect(config).not.toBeUndefined();
  expect(config.issuer_url).toBe("https://localhost");
  expect(config.people[0].pid).toBe("jappleseed");
});

it("loads the data correctly from a YAML file with yaml extension", () => {
  const config = configLoader.getConfig("/tmp/config-loader.yaml");
  expect(config).not.toBeUndefined();
  expect(config.issuer_url).toBe("https://localhost");
  expect(config.people[0].pid).toBe("jappleseed");
});

it("loads the data correctly from a YAML file with yml extension", () => {
  const config = configLoader.getConfig("/tmp/config-loader.yaml");
  expect(config).not.toBeUndefined();
  expect(config.issuer_url).toBe("https://localhost");
  expect(config.people[0].pid).toBe("jappleseed");
});
