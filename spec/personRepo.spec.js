const personRepo = require("../src/personRepo");

const SAMPLE_PEOPLE = {
  "jappleseed": {
    name: "Johnny Appleseed",
    groups: [
      "planters.apples", "migrant",
    ],
  }
};

it("throws when giving it something that isn't an object", () => {
  try {
    personRepo.init([]);
    fail("should have thrown");
  } catch (err) {
    expect(err.message).toContain("must be an object");
  }
});

it("should create and map custom claims correctly when person is found", () => {
  personRepo.init(SAMPLE_PEOPLE);
  const claims = personRepo.getClaims(["email", "email_verified", "groupMembershipUugid", "name", "pid"], "jappleseed");
  expect(claims.email).toBe("jappleseed@vt.edu");
  expect(claims.email_verified).toBe(true);
  expect(claims.groupMembershipUugid).toBe(SAMPLE_PEOPLE.jappleseed.groups);
  expect(claims.uupid).toBe("jappleseed");
  expect(claims.name).toBe("Johnny Appleseed");
});

it("should return empty claims when person not found", () => {
  personRepo.init(SAMPLE_PEOPLE);
  const claims = personRepo.getClaims(["email", "email_verified", "groupMembershipUugid", "name", "pid"], "unknown");
  expect(Object.keys(claims).length).toBe(0);
});