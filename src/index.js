const getProvider = require("./oidcServer");
const configLoader = require("./configLoader");
const personRepo = require("./personRepo");

/////////////// CONFIGURATION ///////////////

if (!process.env.CONFIG_FILE_PATH)
  throw new Error("CONFIG_FILE_PATH must be set");

const port = process.env.PORT || 8080;

const config = configLoader.getConfig(process.env.CONFIG_FILE_PATH);
personRepo.init(config.people);

const oidc = getProvider(config.issuer_url, config.clients, personRepo);

/////////////// SERVER STARTUP ///////////////

let server;
(async () => {
  server = oidc.listen(port, () => {
    console.log(
      `listening on port ${port}, check ${config.issuer_url}/.well-known/openid-configuration`
    );
  });
})().catch((err) => {
  if (server && server.listening) server.close();
  console.error(err);
  process.exitCode = 1;
});

/////////////// CLEAN SHUTDOWN HANDLING ///////////////

const shutdown = () => {
  if (server && server.listening) server.close();
  process.exit();
};
process.on("SIGINT", shutdown);
process.on("SIGTERM", shutdown);
