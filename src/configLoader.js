const fs = require("fs");
const yaml = require("js-yaml");

function getConfig(configSource) {
  if (!configSource.startsWith("/"))
    throw new Error(`Unsupported config source: ${configSource}`);

  const config = _getData(configSource);
  console.log(`Loaded config with ${config.people.length} people and ${config.clients.length} clients`);
  return config;
}

function _getData(configSource) {
  const data = fs.readFileSync(configSource).toString();
  if (configSource.endsWith(".yaml") || configSource.endsWith(".yml"))
    return yaml.safeLoad(data);
  else return JSON.parse(data);
}

module.exports = { getConfig };
