const Provider = require("oidc-provider");

const SUPPORTED_SCOPES = [
  "address",
  "c",
  "facsimileTelephoneNumber",
  "homeFAX",
  "homeMobile",
  "homePager",
  "homePhone",
  "homePostalAddress",
  "l",
  "localFAX",
  "localMobile",
  "localPager",
  "localPhone",
  "localPostalAddress",
  "mailStop",
  "mobile",
  "pager",
  "postalAddress",
  "postalCode",
  "postOfficeBox",
  "st",
  "street",
  "telephoneNumber",
  "virginiaTechAffiliation",
  "userCertificate",
  "userSMIMECertificate",
  "email_verified",
  "creationDate",
  "birthdate",
  "name",
  "gender",
  "email",
  "personType",
  "uid",
  "uupid",
  "virginiaTechID",
  "department",
  "departmentNumber",
  "title",
  "groupMembershipUugid",
  "pidm",
  "udcIdentifier",
  "mail",
  "bannerName",
  "cn",
  "given_name",
  "initials",
  "middle_name",
  "family_name",
  "instantMessagingID",
  "labeledURI",
  "mailExternalAddress",
  "campus",
  "college",
  "lastEnrollmentTerm",
  "major",
  "nextEnrollmentTerm",
  "studentLevelCode",
  "suppressDisplay",
  "suppressedAttribute",
  "openid",
];

function getProvider(issuerUrl, clientConfig, personRepo) {
  clientConfig.forEach((client) => {
    if (client.id === undefined)
      throw new Error("Every client needs an `id` attribute");
    if (client.redirect_uris === undefined)
      throw new Error("Every client needs a `redirect_uris` attribute");
  });

  const clients = clientConfig.map((c) => ({
    application_type: "web",
    client_id: c.id,
    client_secret: c.secret,
    redirect_uris: c.redirect_uris,
    token_endpoint_auth_method: c.secret ? "client_secret_basic" : "none"
  }));

  const oidcConfig = {
    findAccount: (_, sub) => {
      if (!personRepo.isValidPerson(sub))
        return undefined;

      return {
      accountId: sub,
      claims: (_, scope) => {
        return {
          sub,
          ...personRepo.getClaims(scope.split(" "), sub),
        };
      },
    };
    },
    features: {
      // Enables built-in login screens
      devInteractions: { enabled: true },
      registration: { enabled: false },
      revocation: { enabled: false },
      claimsParameter: { enabled: false },
      requestObjects: { enabled: false },
      sessionManagement: { enabled: false },
    },
    formats: {
      default: "jwt",
      AccessToken: "jwt",
    },
    claims: SUPPORTED_SCOPES.reduce(
      (acc, cur) => ({ ...acc, [cur]: [cur] }),
      {}
    ),
    clients,
    clientDefaults: {
      grant_types: ["authorization_code"],
      id_token_signed_response_alg: "RS256",
      response_types: ["code"],
      token_endpoint_auth_method: "none",
    },
    conformIdTokenClaims: true,
    issueRefreshToken: () => true,
    subjectTypes: ["pairwise"],
    scopes: SUPPORTED_SCOPES,
    whitelistedJWA: {
      idTokenSigningAlgValues: ["RS256"],
      userinfoSigningAlgValues: ["RS256"],
      requestObjectSigningAlgValues: [],
    },
  };

  const provider = new Provider(issuerUrl, oidcConfig);
  provider.proxy = true;
  return provider;
}

module.exports = getProvider;
