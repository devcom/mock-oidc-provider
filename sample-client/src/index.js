const path = require("path");
const express = require("express");
const expressSession = require("express-session");
const cookieParser = require("cookie-parser");
const passport = require("passport");
const { Issuer, Strategy } = require("openid-client");
const jwt_decode = require("jwt-decode");

if (!process.env.OIDC_PROVIDER)
  throw new Error("OIDC_PROVIDER must be specified");
if (!process.env.OIDC_CLIENT_ID)
  throw new Error("OIDC_CLIENT_ID must be specified");
if (!process.env.OIDC_CLIENT_SECRET)
  throw new Error("OIDC_CLIENT_SECRET must be specified");
if (!process.env.PUBLISHED_HOSTNAME)
  throw new Error("PUBLISHED_HOSTNAME must be specified");
if (!process.env.OIDC_SCOPES)
  throw new Error("OIDC_SCOPES must be specified");


async function startup() {
  const app = express();

  app.set("views", path.join(__dirname, "views"));
  app.set("view engine", "ejs");
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
  
  app.use(expressSession({
    secret: "super secret",
    resave: false,
    saveUninitialized: true,
  }));

  passport.serializeUser((user, done) => done(null, user));
  passport.deserializeUser((user, done) => done(null, user));
  
  const { client, strategy } = await getOidcPassportStrategyAndClient(process.env.OIDC_PROVIDER);
  passport.use("oidc", strategy);

  if (process.env.OIDC_SPA_CLIENT_ID) {
    app.get("/spa", (req, res) => {
      res.render("spa", {
        clientId: process.env.OIDC_SPA_CLIENT_ID,
        redirectUri: `${process.env.PUBLISHED_HOSTNAME}/spa`,
        authorizationEndpoint: strategy._issuer.authorization_endpoint,
        tokenEndpoint: strategy._issuer.token_endpoint,
        scopes: process.env.OIDC_SCOPES,
      });
    });
  }

  app.use(passport.initialize());
  app.use(passport.session());
  
  app.get("/", (req, res, next) => passport.authenticate("oidc")(req, res, next));
  app.get("/auth/callback", (req, res, next) => passport.authenticate("oidc", {successRedirect: '/complete', failureRedirect: '/error'})(req, res, next));
  app.get("/complete", (req, res) => {
    if (!req.user)
      return res.redirect("/");

    const decodedIdToken = jwt_decode(req.user.authCode.id_token);
    res.render("complete", { user: req.user.authCode, decodedIdToken, userInfo : req.user.userInfo, })
  });

  app.listen(3000, () => console.log("Listening on port 3000"));
}


async function getOidcPassportStrategyAndClient(providerUrl, failCount = 0) {
  try {
    const issuer = await Issuer.discover(providerUrl);
    const client = new issuer.Client({
      client_id: process.env.OIDC_CLIENT_ID,
      client_secret: process.env.OIDC_CLIENT_SECRET,
      redirect_uris: [`${process.env.PUBLISHED_HOSTNAME}/auth/callback`],
      response_types: ["code"],
    });

    return {
      client,
      strategy: new Strategy(
        {
          client,
          usePKCE: false,
          params: {
            scope: process.env.OIDC_SCOPES,
          },
        }, 
        (tokenSet, userInfo, done) => {
          done(null, { authCode: tokenSet, userInfo, })
        }
      )
    };
  } catch (err) {
    if (failCount >= 5)
      throw err;
    await (new Promise((acc) => setTimeout(acc, 2000)));
    return getOidcPassportStrategyAndClient(providerUrl, failCount + 1);
  }
}

startup()
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });

process.on("SIGINT", () => process.exit());
process.on("SIGTERM", () => process.exit());