# Sample OIDC Client

This client performs an OIDC login and displays the information about the auth code and the user.

**NOTE:** Since this exposes the raw access and refresh tokens, this tool should only be used in development and debugging.

## Configuration

The following environment variables are required to be set. Since this is intended to be used for dev/debugging, the client secret is specified using environment variables.

| Env Var Name | Description |
|--------------|-------------|
| `OIDC_CLIENT_ID` | The id of the client |
| `OIDC_CLIENT_SECRET` | The secret of the OIDC client |
| `OIDC_PROVIDER` | The full URL to the root of the provider. This will be used to auto-discover the various endpoints |
| `PUBLISHED_HOSTNAME` | The URL this client is accessible via the browser. Used in generating redirect URIs |
| `OIDC_SCOPES` | The scopes to request, space delimited |
